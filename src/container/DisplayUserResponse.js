import React from "react";
import { View, Text, StyleSheet } from "react-native";

const DisplayUserResponse = ({ item, index, data1 }) => {

    var youranswer, correctanswer;

    for (var i of data1.correctAnswersList) {
        if (item.question === i.quest) {
            correctanswer = i.crctopt;
        }
    }

    for (var i of data1.yourAnswersList) {
        if (item.question === i.quest) {
            youranswer = i.youroption;
        }
    }

    return (
        <View>
            <View style = {styles.questionOptionsContainer}>
                <View>
                    <Text style={{ marginVertical: 8, fontSize:18, fontWeight: '500' }} >{index + 1}. {item.question}</Text>
                    <Text style={ styles.text } >a) {item.options[0].option}</Text>
                    <Text style={ styles.text } >b) {item.options[1].option}</Text>
                    <Text style={ styles.text } >c) {item.options[2].option}</Text>
                    <Text style={ styles.text } >d) {item.options[3].option}</Text>
                </View>
                <Text style={styles.rightWrongLogo}>{youranswer === correctanswer ? '\u2705' : '\u274C'}</Text>
            </View>
            <Text style={styles.viewAnswer}>Response: {youranswer} | Answer: {correctanswer} </Text>
        </View>

    )
}

const styles = StyleSheet.create({
    questionOptionsContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    text: {
        marginBottom: 8,
        marginLeft: 15,
        fontSize: 16,
        fontWeight: '400'
    },
    viewAnswer: {
        textAlign: 'center',
        fontSize: 16
    },
    rightWrongLogo: {
        textAlign: 'right',
        marginRight: 10,
    }
})

export default DisplayUserResponse;
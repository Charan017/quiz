import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from './HomeScreen';
import TestScreen from './TestScreen';
import ResultScreen from './ResultScreen';
import AllScores from './AllScores';
import UserResponse from './UserResponse';
import ViewAllScores from './ViewAllScores';

const Stack = createStackNavigator();

const HomeStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name="HomeScreen" component={HomeScreen} options={{ headerShown: false }} />
      <Stack.Screen name="TestScreen" component={TestScreen} options={{ headerShown: false }} />
      <Stack.Screen name="ResultScreen" component={ResultScreen} options={{ headerShown: false }} />
      <Stack.Screen name="AllScores" component={AllScores} options={{ headerShown: false }} />
      <Stack.Screen name="UserResponse" component={UserResponse} options={{ headerShown: false }} />
      {/* <Stack.Screen name="ViewAllScores" component={ViewAllScores} options={{ headerShown: false }} /> */}

    </Stack.Navigator>
  );
}

export default HomeStack;
import React, { useState, useEffect } from "react";
import { Text, View, StyleSheet, FlatList } from "react-native";
import AsyncStorage from '@react-native-async-storage/async-storage';
import ViewAllScores from "./ViewAllScores";

const AllScores = ({navigation}) => {

    const [history, setHistory] = useState([])
    const callingViewAllScreen = (item)=>{
        navigation.navigate('UserResponse', {
            data1: item
        })
    }

    const getData = async () => {
        try {
            const savedData = await AsyncStorage.getItem('key')
            var a = JSON.parse(savedData);
            setHistory(a)
        }
        catch (error) {
            console.log(error);
        }
    }

    // console.log(history);

    const sortScores = () => {
        for (var i = 0; i < history.length; i++) {
            for (var j = i + 1; j < history.length; j++) {
                if (history[i].userScore < history[j].userScore) {
                    var temp = history[j];
                    history[j] = history[i];
                    history[i] = temp;
                }
            }
        }
    }

    useEffect(() => {
        getData();
    }, []);

    sortScores();

    return (
        <View style={{ flex: 1 }}>
            <View style={{ marginTop: 20 }} />
            <View style={{ flex: 1 }}>
                <Text style={styles.title}>Previous Scores</Text>
                <View style={styles.nameScoreTitle}>
                    <Text style={styles.nameScoreText} >Name</Text>
                    <Text style={styles.nameScoreText} >Score</Text>
                </View>
                <FlatList
                    data={history}
                    renderItem={({ item }) => 
                        <ViewAllScores item={item} callingViewAllScreen ={callingViewAllScreen} />
                    }
                />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    title: {
        textAlign: 'center',
        marginTop: 20,
        fontSize: 30,
        color: 'black'
    },
    nameScoreTitle: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 20,
        marginHorizontal: 50,
        marginBottom: 10
    },
    nameScoreText: {
        fontSize: 20,
        color: 'black'
    }
})

export default AllScores;
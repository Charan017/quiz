import React, { useEffect, useState } from "react";
import { StyleSheet, View, Text, Image, BackHandler, TouchableOpacity, Alert } from "react-native";
import ModalScreen from "./ModalScreen";

const HomeScreen = ({ navigation }) => {

    const [modalOpen, setModalOpen] = useState(false);

    // const backActionHandler = () => {
    //     Alert.alert('', 'Are you sure want to exit the app?', [
    //         {
    //             text: 'No',
    //             onPress: () => { },
    //             style: 'cancel'
    //         },
    //         {
    //             text: 'Yes',
    //             onPress: () => BackHandler.exitApp(),
    //         }
    //     ]);
    //     return true;
    // }

    // useEffect(() => {
    //  const listener =  BackHandler.addEventListener("hardwareBackPress", backActionHandler);
    //    return () => {
    //     listener.remove()
    //    }
    // })

    const handleModal = (modalflag) => {
        setModalOpen(modalflag);
    }

    return (
        // <View style = {{flex: 1}}>
        // <View style={{ marginTop: 20 }} />
        // </View>

        < View style={styles.container} >
            <Text style={styles.QuizTitle}>Quiz App</Text>
            <Image style={styles.logo} source={require('../assets/quizImage.png')} />
            <View style={styles.touchableContainer}>
                <ModalScreen modalOpen={modalOpen} handleModal={handleModal} navigation={navigation} />
                <TouchableOpacity style={styles.takeTestButton} onPress={() => setModalOpen(!modalOpen)} >
                    <Text style={styles.takeTestText}>Take Test</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => navigation.navigate('AllScores')} >
                    <Text style={{ fontSize: 20 }}>View All Scores</Text>
                </TouchableOpacity>
            </View>
        </View >

    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-around',
        backgroundColor: '#91EAE4'
    },
    QuizTitle: {
        fontSize: 35,
        fontWeight: 'bold'
    },
    logo: {
        width: 150,
        height: 150,
        borderRadius: 40
    },
    touchableContainer: {
        alignItems: 'center'
    },
    takeTestButton: {
        backgroundColor: '#ddd',
        marginBottom: 30,
        borderRadius: 5
    },
    takeTestText: {
        marginHorizontal: 100,
        paddingVertical: 15,
        color: '#ggg',
        fontSize: 20,
    },
});

export default HomeScreen;
import React, { useState, useEffect } from "react";
import { Text, View, TouchableOpacity, StyleSheet, FlatList, } from "react-native";
import AsyncStorage from '@react-native-async-storage/async-storage';
import OutputScreen from "./OutputScreen";

const ResultScreen = ({ route, navigation }) => {

    // const [userResponse, setUserResponse] = useState({});
    const [userData, setUserData] = useState([]);
    var questionNumber = 0;

    const fun = () => {
        setData();
    }

    const setData = async () => {
        try {
            const jsonValue = JSON.stringify(userData)
            AsyncStorage.setItem('key', jsonValue);
        }
        catch (error) {
            console.log(error);
        }
    }

    const getData = async () => {
        try {
            const savedData = await AsyncStorage.getItem('key')
            if (savedData != null) {
                var a = JSON.parse(savedData);
                setUserData([...a, { userName: route.params.userDetails, userScore: route.params.score, questions: route.params.res, correctAnswersList: route.params.correctAnswerList, yourAnswersList: route.params.yourAnswerList }])
            }
            else {
                setUserData((prevData) => {
                    return [...prevData, { userName: route.params.userDetails, userScore: route.params.score,  questions: route.params.res, correctAnswersList: route.params.correctAnswerList, yourAnswersList: route.params.yourAnswerList  }]
                })
            }
        }
        catch (error) {
            console.log(error);
        }
    }

    useEffect(() => {
        if (userData?.length > 0) {
            setData();
        }
    }, [userData])

    useEffect(() => {
        getData();
    }, []);

    // console.log(userData[0]["correctAnswersList"])

    return (
        <View style={{ flex: 1, margin: 5 }}>
            <View style={{ flex: 1 }}>
                <Text style={styles.headerText} >Thanks for taking the test.</Text>
                <Text style={styles.headerText} >Your Score: {route.params.score}/{route.params.resultantArray.length}</Text>
                <Text style={styles.headerText} >Your Response:</Text>
            </View>
            <View style={{ flex: 8, }}>
                <FlatList
                    data={route.params.res}
                    renderItem={({ item }) => {
                        questionNumber += 1;
                        return <OutputScreen item={item} questionNumber={questionNumber} correctAnswerList={route.params.correctAnswerList} yourAnswerList={route.params.yourAnswerList} />
                    }}
                />
            </View>
            <View style={styles.takeTestViewScoresContainer}>
                <TouchableOpacity style={styles.takeTestViewScoresButtons} onPress={() => navigation.navigate('HomeScreen')} >
                    <Text style={styles.takeTestViewScoresText} >Take Test Again</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.takeTestViewScoresButtons} onPress={() => { fun, navigation.navigate('AllScores', { userData }) }}>
                    <Text style={styles.takeTestViewScoresText} >View All Scores</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    headerText: {
        marginTop: 3,
        color: 'black'
    },
    takeTestViewScoresContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    takeTestViewScoresButtons: {
        backgroundColor: 'aqua',
        padding: 10,
        borderRadius: 10
    },
    takeTestViewScoresText: {
        fontSize: 15,
        color: 'black'
    }
})

export default ResultScreen;
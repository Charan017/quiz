import React from "react";
import { View ,Text, FlatList} from "react-native";
import data from "./Data";
import DisplayUserResponse from "./DisplayUserResponse";

const UserResponse = ({route}) => {
const {data1} = route.params;
    return (
        <View style = {{marginHorizontal: 10}}>
            <View style = {{ marginTop: 20}}></View>
            <View style = {{flexDirection: 'row', justifyContent: 'space-between', marginVertical: 10, }}>
                <Text style = {{fontSize:20}}>Name: {data1.userName}</Text>
                <Text style = {{fontSize:20}}>Score: {data1.userScore}</Text>
            </View>
            <FlatList
            data={data.questionsData}
            renderItem = {({item,index}) => {
                return <DisplayUserResponse item = {item} index = {index} data1 = {data1} />
            }}
            />
        </View>
        
    )
}

export default UserResponse;
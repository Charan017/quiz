import React from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import RadioButton from "./radioButtons";

const OptionsScreen = ({ item, buttonHandler, checkOptionId, temp, saveOptions }) => {

    return (
        <TouchableOpacity onPress={() => { buttonHandler(item.key) }}>
            <View style={styles.container}>
                <View style={styles.radioButtonView}>
                    <RadioButton index={item.key} checkOptionId={checkOptionId}  saveOptions = {saveOptions} temp = {temp}  />
                </View>
                <View>
                    <Text style={styles.radioButtonText}>{item.value}</Text>
                </View>
            </View>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row', 
        marginVertical: 10
    },
    radioButtonView: {
        marginRight: 10
    },
    radioButtonText: {
        fontSize: 20,
        color: 'black',
    },
})

export default OptionsScreen;
import React, { useEffect, useState } from 'react';
import { TouchableOpacity, View, Text, StyleSheet, FlatList, Alert, } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import OptionsScreen from './OptionsScreen';
import data from './Data';

var check = 0;
var temp = [];

const TestScreen = ({ route, navigation }) => {
    const [questionNo, setQuestionNo] = useState(1);
    const [buttonChange, setButtonChange] = useState('Next');
    const [checkOptionId, setCheckOptionId] = useState('');
    const [saveOptions, setSaveOptions] = useState({});
    const [result, setResult] = useState([]);
    const [enableNext, setEnableNext] = useState(true);
    const [questionsAnswered, setQuestionsAnswered] = useState([]);
    const [enableAnsweredQuestions, setEnableAnsweredQuestions] = useState(false);
    const userDetails = route.params.userName;
    const nav = useNavigation();

    var score = 0;
    const reverseResult = result.reverse();
    var resultantArray = [];
    var res = [];
    const correctAnswerList = [];
    const yourAnswerList = [];

    const optionsList = [
        {
            value: data.questionsData[questionNo - 1].options[0].option,
            key: data.questionsData[questionNo - 1].options[0].option_id,
        },
        {
            value: data.questionsData[questionNo - 1].options[1].option,
            key: data.questionsData[questionNo - 1].options[1].option_id,
        },
        {
            value: data.questionsData[questionNo - 1].options[2].option,
            key: data.questionsData[questionNo - 1].options[2].option_id,
        },
        {
            value: data.questionsData[questionNo - 1].options[3].option,
            key: data.questionsData[questionNo - 1].options[3].option_id,
        },
    ];

    useEffect(
        () =>
            nav.addListener('beforeRemove', (event) => {
                event.preventDefault();

                Alert.alert('', 'Are you sure you want to exit from exam?', [
                    { text: 'No', style: 'cancel', onPress: () => { } },
                    {
                        text: 'Yes',
                        style: 'destructive',
                        onPress: () => nav.dispatch(event.data.action),
                    },
                ]);
            }),
        [nav]
    );

    const ResultScreenScore = () => {
        for (var i of resultantArray) {
            if (i.correctAns === i.yourAns) {
                score += 1;
            }
        }
    };

    const modifyData = () => {
        for (var i of reverseResult) {
            if (resultantArray.length === 0) {
                resultantArray.push(i);
                continue;
            }
            var flag = true;
            for (var j of resultantArray) {
                if (j.que_id === i.que_id) {
                    flag = false;
                }
            }
            if (flag) {
                resultantArray.push(i);
            }
        }
    };

    const questionsAndOptions = () => {
        for (var i of data.questionsData) {
            for (var j of resultantArray) {
                if (i.question_id === j.que_id) {
                    res.push({
                        ques: i.question, option_a: i.options[0].option, option_b: i.options[1].option, option_c: i.options[2].option, option_d: i.options[3].option, yourAnswer: j.yourAns, CorrectAnswerId: j.correctAns,
                    });
                }
            }
        }
    };

    const correctAnswerFunc = () => {
        for (var i of data.questionsData) {
            for (var j of i.options) {
                for (var k of resultantArray) {
                    if (j.option_id === k.correctAns) {
                        correctAnswerList.push({ quest: i.question, crctopt: j.option });
                    }
                }
            }
        }
    };

    const yourAnswerFunc = () => {
        for (var i of data.questionsData) {
            for (var j of i.options) {
                for (var k of resultantArray) {
                    if (j.option_id === k.yourAns) {
                        yourAnswerList.push({ quest: i.question, youroption: j.option });
                    }
                }
            }
        }
    };

    modifyData();
    ResultScreenScore();
    questionsAndOptions();
    correctAnswerFunc();
    yourAnswerFunc();

    const buttonHandler = (text) => {
        if (text.length !== 0) {
            check += 1;
        }
        setCheckOptionId(text);

        for (var i in data.questionsData) {
            for (var j in data.questionsData[0].options) {
                if (data.questionsData[i].options[j].option_id === text) {
                    temp.push(data.questionsData[i].question_id);
                    setSaveOptions((prevOptions) => {
                        prevOptions[temp[questionNo - 1]] = text;
                        return prevOptions;
                    });
                }
            }
        }

        setResult((prevResult) => {
            const newData = [
                ...prevResult,
                {
                    que_id: data.questionsData[questionNo - 1].question_id,
                    yourAns: text,
                    correctAns: data.questionsData[questionNo - 1].correct_option,
                },
            ];
            return newData;
        });

        setEnableNext(false);
    };

    const nextHandler = () => {
        if (questionsAnswered.includes(questionNo)) {
            setEnableNext(false);
            setQuestionNo(questionNo + 1);
        } else {
            if (data.questionsData.length !== questionNo && check !== 0) {
                check = 0;
                setQuestionsAnswered((prevQuestionsAnswered) => {
                    return [...prevQuestionsAnswered, questionNo];
                });
                setQuestionNo(questionNo + 1);
            }
            setEnableNext(true);
        }
        if (questionNo === data.questionsData.length - 1) {
            setButtonChange('Submit');
        }
    };

    const prevHandler = () => {
        setEnableNext(false);
        if (questionNo !== 1) {
            setQuestionNo(questionNo - 1);
        }
        if (questionNo === 6) {
            setButtonChange('Next');
        }
    };

    const hideAnsweredQuestions = () => {
        setEnableAnsweredQuestions(!(enableAnsweredQuestions));
    }
    const abc = (index) => {
        setQuestionNo(index + 1)
        setEnableNext(false)
    }


    return (
        <View style={{ flex: 1 }}>
            <View style={{ marginTop: 20 }} />
            <View style={styles.container}>
                <View style={styles.titleContainer}>
                    <Text style={styles.titleName}>Hello, {userDetails}</Text>
                    <Text style={styles.titleTest}>This is your Test</Text>
                </View>
                <View style={{}}>
                    <View>
                        <TouchableOpacity style={{ alignSelf: 'flex-start' }} onPress={() => hideAnsweredQuestions()}>
                            <Text style={{ fontSize: 16 }}>QuestionsAnswered</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ alignItems: 'center' }} >
                        {enableAnsweredQuestions && <FlatList
                            horizontal
                            data={questionsAnswered}
                            renderItem={({index }) =>

                                <TouchableOpacity onPress={() => {return abc(index)}}>
                                    <View style={{ backgroundColor: '#eee', width: 40, height: 40, borderRadius: 20, alignItems: 'center', justifyContent: 'center', marginRight: 5 }}>
                                        <Text>{index + 1}</Text>
                                    </View>
                                </TouchableOpacity>
                            }
                        />}
                    </View>
                </View>
                <View style={styles.questionsView}>
                    <View>
                        <Text style={styles.questionText}>{questionNo}. {data.questionsData[questionNo - 1].question}</Text>
                        <FlatList
                            data={optionsList}
                            renderItem={({ item }) => (
                                <OptionsScreen item={item} buttonHandler={buttonHandler} checkOptionId={checkOptionId} saveOptions={saveOptions} temp={temp[questionNo - 1]} />
                            )}
                        />
                    </View>
                    <View style={styles.prevNextContainer}>
                        <TouchableOpacity style={styles.prevNextButtons} onPress={prevHandler}>
                            <Text style={styles.prevNextText}>Prev</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.prevNextButtons} disabled={enableNext} onPress={
                            buttonChange === 'Submit' ? () => {
                                setResult((prevResult) => {
                                    return [
                                        ...prevResult,
                                        {
                                            que_id: data.questionsData[questionNo - 1].question_id,
                                            yourAns: checkOptionId,
                                            correctAns: data.questionsData[questionNo - 1].correct_option,
                                        },
                                    ];
                                });
                                navigation.navigate('ResultScreen', { userDetails, score, resultantArray, res, correctAnswerList, yourAnswerList });
                            } : nextHandler}
                        >
                            <Text style={styles.prevNextText}>{buttonChange}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    titleContainer: {
        alignItems: 'center',
        flex: 1,
        flex: 1,
        justifyContent: 'center',
        backgroundColor: 'white',
    },
    titleName: {
        fontSize: 20,
        fontWeight: '600',
        color: 'black',
    },
    titleTest: {
        fontSize: 16,
        color: 'black',
    },
    questionsView: {
        flex: 9,
        justifyContent: 'center',
        marginHorizontal: 15,
    },
    questionText: {
        fontSize: 25,
        color: 'black',
    },
    prevNextContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 15,
    },
    prevNextButtons: {
        backgroundColor: 'aqua',
        borderRadius: 5,
    },
    prevNextText: {
        fontSize: 20,
        paddingHorizontal: 20,
        paddingVertical: 5,
    },
});

export default TestScreen;

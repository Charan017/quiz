import React from 'react'
import { View, StyleSheet } from 'react-native'

const RadioButton = ({ index, saveOptions, temp }) => {

    return (
        <View style={styles.container}>
            <View style={styles.outerCircle}>
                <View style={{
                    ...styles.innerCircle, ...{
                        backgroundColor: saveOptions[temp] === index ? 'black' : 'white',
                        borderColor: saveOptions[temp] === index ? 'black' : 'white'
                    }
                }}>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        justifyContent: 'center',
    },
    outerCircle: {
        borderWidth: 2,
        width: 22,
        height: 22,
        borderRadius: 11,
        justifyContent: 'center',
    },
    innerCircle: {
        width: 14,
        height: 14,
        borderWidth: 2,
        borderRadius: 7,
        alignSelf: 'center',
    },
})

export default RadioButton;
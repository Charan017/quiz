import React, { useState } from "react";
import { View, Text, StyleSheet } from "react-native";

const OutputScreen = ({ item, questionNumber, correctAnswerList, yourAnswerList }) => {

    for (var i of correctAnswerList) {
        if (item.ques === i.quest) {
            var correctanswer = i.crctopt;
        }
    }

    for (var i of yourAnswerList) {
        if (item.ques === i.quest) {
            var youranswer = i.youroption;
        }
    }

    return (
        <View>
            <View style={styles.questionOptionsContainer}>
                <View style={{ flex: 0.8 }}>
                    <Text style={{ marginTop: 10, fontWeight: '500' }}>{questionNumber}. {item.ques}</Text>
                    <Text style={styles.text} >a) {item.option_a}</Text>
                    <Text style={styles.text} >b) {item.option_b}</Text>
                    <Text style={styles.text} >c) {item.option_c}</Text>
                    <Text style={styles.text} >d) {item.option_d}</Text>
                </View>
                <Text style={styles.rightWrongLogo}>{item.yourAnswer === item.CorrectAnswerId ? '\u2705' : '\u274C'}</Text>
            </View>
            <Text style={styles.viewAnswer}>Response: {youranswer} | Answer: {correctanswer} </Text>
        </View>
    )
}

const styles = StyleSheet.create({
    questionOptionsContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    text: {
        marginBottom: 8,
        marginLeft: 15,
        fontSize: 16,
        fontWeight: '400'
    },
    viewAnswer: {
        textAlign: 'center',
        fontSize: 16
    },
    rightWrongLogo: {
        textAlign: 'right',
        marginRight: 10,
    }
})

export default OutputScreen;
import React, { useState, useEffect } from 'react';
import { StyleSheet, View, Text, TouchableOpacity, TouchableWithoutFeedback, Modal, TextInput, Keyboard, } from 'react-native';

const ModalScreen = ({ modalOpen, handleModal, navigation }) => {

    const [userName, setUserName] = useState('');
    const [enable, setEnable] = useState(true);

    const setName = (text) => {
        setUserName(text);
        if (text.trim().length > 2) {
            setEnable(false);
        }
    };

    return (
        <Modal visible={modalOpen} animationType="slide" transparent>
            <TouchableWithoutFeedback onPress={() => { Keyboard.dismiss() }}>
                <View style={styles.centeredView}>
                    <View style={styles.innerContainer}>
                        <Text style={styles.text}>Please Enter Your Name</Text>
                        <TextInput style={styles.inputText} keyboardType="default" onChangeText={setName} />
                        <View style={styles.prevNextContainer}>
                            <TouchableOpacity style={styles.prevNextButtons} onPress={() => handleModal(!modalOpen)}>
                                <Text style={styles.prevNextText}>Cancel</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={styles.prevNextButtons}
                                disabled={enable}
                                onPress={
                                    userName.trim().length > 2
                                        ? () => {
                                            navigation.navigate('TestScreen', { userName }),
                                                handleModal(!modalOpen);
                                        }
                                        : null
                                }>
                                <Text style={styles.prevNextText}>Ok</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </TouchableWithoutFeedback>
        </Modal>
    );
};

const styles = StyleSheet.create({
    innerContainer: {
        backgroundColor: 'white',
        paddingHorizontal: 20,
        paddingVertical: 20,
        borderRadius: 10
    },
    text: {
        textAlign: 'center',
        fontSize: 25,
        color: 'black',
        marginBottom: 5,
    },
    inputText: {
        borderBottomWidth: 1,
        fontSize: 20
    },
    centeredView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(0,0,0,0.4)',
    },
    prevNextContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 25,
    },
    prevNextButtons: {
        borderRadius: 5,
        backgroundColor: '#eee',
    },
    prevNextText: {
        fontSize: 20,
        paddingHorizontal: 20,
        paddingVertical: 5,
        color: 'black',
    },
});

export default ModalScreen;

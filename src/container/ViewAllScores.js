import React from "react";
import { Text, View, StyleSheet } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";

const ViewAllScores = ({ item, callingViewAllScreen}) => {
    return (
        <View style={styles.container}>
            <TouchableOpacity onPress={() => callingViewAllScreen(item)}>
                <View style={{ flexDirection: 'row', flex: 1, justifyContent: 'space-between', height: 40, alignItems: 'center' }}>
                    <Text style={styles.nameText} >{item.userName}</Text>
                    <Text style={styles.scoreText} >{item.userScore}</Text>
                </View>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        // marginTop: 20,
        marginHorizontal: 50,
        marginBottom: 5
    },
    nameText: {
        fontSize: 15,
        color: 'black',
        // marginTop: 8
    },
    scoreText: {
        fontSize: 15,
        color: 'black',
        // marginTop: 8,
        marginRight: 20
    }
})

export default ViewAllScores;